import 'package:flutter/cupertino.dart';

class Juego {
  final int id;
  final String titulo;
  final String numerosagas;
  final String temporadas;
  final String preciogame;
  final String genero;


  Juego({
    required this.id,
    required this.titulo,
    required this.numerosagas,
    required this.temporadas,
    required this.preciogame,
    required this.genero,

  });
}

