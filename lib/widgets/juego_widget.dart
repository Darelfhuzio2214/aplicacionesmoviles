import 'package:flutter/material.dart';
import '../constants.dart';
import '../model/juego.dart';

class GameWidget extends StatelessWidget {
  final Juego juego;
  final Function onTap;


  const GameWidget(
      {Key? key,
      required this.juego,
      required this.onTap,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
      },
      onLongPress: () {
      },
      onTap: () {
        onTap(juego);
      },
      
      child: Container(
      
        padding: const EdgeInsets.all(10.0),
        height: 80.0,

        child: Row(
       
          
          children: <Widget> [
            
            
            Expanded(
              flex: 1,
              child: Container(
                height: 12.0,
                
              ),
              
            ),
            Expanded(
              flex: 9,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text('TÍTULO GAME : ${juego.titulo}', style: fromTextStyle),
                   Text('PRECIO :${juego.preciogame}', style: subjectTextStyle),
                  Text('GÉNERO : ${juego.genero}',
                      style: fromTextStyle),
                 
                ],
              ),
            )
       
          ],
        ),
      ),
    );
  }
}
