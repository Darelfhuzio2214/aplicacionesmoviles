import 'package:flutter/material.dart';

import '../constants.dart';
import '../model/juego.dart';


class DetailScreen extends StatelessWidget {
 
  final Juego juego;

  const DetailScreen({Key? key, required this.juego}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(juego.titulo),
        ),
        body: Container(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('TEMPORADAS: ${juego.temporadas}', style: fromTextStyle),
              const SizedBox(height: 10.0),
              const Divider(color: primaryColor),
              const SizedBox(height: 10.0),
              const SizedBox(height: 5.0),
              Text('GÉNERO : ${juego.genero}', style: subjectTextStyle),
              const SizedBox(height: 10.0),
              const Divider(color: primaryColor),
              Text('PRECIO : ${juego.preciogame}', style: subjectTextStyle),
              const Divider(color: primaryColor),
              const SizedBox(height: 10.0),
              const SizedBox(height: 5.0),
              Text('NÚMERO SAGAS: ${juego.numerosagas}', style: subjectTextStyle),
              const SizedBox(height: 20.0),
                 ElevatedButton(
                style: ElevatedButton.styleFrom(
                primary: Color.fromARGB(184, 255, 2, 2), // background
                onPrimary: Color.fromARGB(255, 255, 253, 253), // foreground
              ),
              onPressed: () { 
                Navigator.pop(context);
              },
              child: Text('Done'),
)
            ],
            
          ),
          
        ))
        ;
    }
}
