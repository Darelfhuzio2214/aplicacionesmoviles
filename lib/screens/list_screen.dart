import 'package:flutter/material.dart';

import '../constants.dart';
import '../model/backend.dart';
import '../widgets/juego_widget.dart';
import 'detail_screen.dart';
import '../model/juego.dart';

class ListScreen extends StatefulWidget {


  const ListScreen({Key? key, required this.title}) : super(key: key);


  final String title;

  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {


  
  
  var games = Backend().getJuegos();
 

  void showDetail( Juego juego) {
    Navigator.push(context, MaterialPageRoute(builder: (context) {
      return DetailScreen( juego: juego,);
    }));
    
    setState(() {
 
      games = Backend().getJuegos();
    });
  }

  @override
  Widget build(BuildContext context) {
 
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title), 

       
      ),


      
      body: ListView.separated(
        itemCount: games.length,
        separatorBuilder: (BuildContext context, int index) => const Divider(
          color: primaryColor,
          indent: 40.0,
          endIndent: 20.0,
        ),
          itemBuilder: (BuildContext context, int index) => GameWidget(
          juego: games[index],
          onTap: showDetail,  
    
  
        ),
      
          
      ),  

    );  
  }
}
